#!/usr/bin/env python3
import csv
import os
import logging
import concurrent.futures
import youtube_dl
import argparse


global parent_dir
global inputfile


class ydl_logger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)


def read_channels():
    try:
        with open(inputfile, newline='') as csvfile:
            global channels
            channels = list(csv.reader(csvfile, delimiter=',', quotechar='|'))
    except FileNotFoundError:
        print('File does not exist.')
        exit()


def create_folders():
    try:
        os.mkdir(parent_dir)
        print("Root directory '% s' created" % parent_dir)
    except FileExistsError:
        print("Root directory '% s' already exists" % parent_dir)

    for channel in channels:
        try:
            os.mkdir(os.path.join(parent_dir, channel[0]))
            print("Directory '% s' created" % os.path.join(parent_dir, channel[0]))
        except FileExistsError:
            print("Directory '% s' already exists" % os.path.join(parent_dir, channel[0]))


def my_hook(d):
    if d['status'] == 'finished':
        print('Done downloading '+d['filename'])
    elif d['status'] == 'downloading':
        print('Downloading '+d['filename'])
    elif d['status'] == 'error':
        print('Error while downloading file')


def downloader(channel):
    logging.info("Downloading %s's videos: starting thread", channel[0])

    ydl_opts = {
        'format': 'best',
        'continuedl': True,  # Force resume of partially downloaded files
        'ignoreerrors': True,  # Continue on download errors, for example to skip unavailable videos
        'nooverwrites': True,  # Do not overwrite files
        'writesubtitles': True,
        'allsubtitles': True,
        'postprocessors': [{
            'key': 'FFmpegEmbedSubtitle',  # To embed subtitles into output file
        }],
        'outtmpl': f'{parent_dir}/{channel[0]}/%(upload_date)s_%(title)s_%(id)s.%(ext)s',
        'external_downloader': 'aria2c',
        'external_downloader_args': ["-c", "-j3", "-x3", "-s3", "-k", "1M"],
        'logger': ydl_logger(),
        'progress_hooks': [my_hook],
    }

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([channel[1]])

    logging.info("Thread %s: finishing", channel[0])


def create_threads():
    with concurrent.futures.ThreadPoolExecutor(max_workers=3) as executor:
        executor.map(downloader,  iter(channels))


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Input file, output file and download options')
    parser.add_argument('-i', dest='inputfile', type=str, default="./channels.csv", help='CSV file containing a list of channels')
    parser.add_argument('-o', dest='parent_dir', type=str, default="./youtube-backup", help='output file')
    args = parser.parse_args()

    parent_dir = args.parent_dir
    inputfile = args.inputfile

    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%H:%M:%S")

    read_channels()
    create_folders()
    create_threads()
